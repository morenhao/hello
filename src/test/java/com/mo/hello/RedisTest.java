package com.mo.hello;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import redis.clients.jedis.Jedis;

import java.util.Map;
import java.util.Set;

@SpringBootTest
public class RedisTest {

    @Test
    void testJedis() {
        Jedis jedis = new Jedis("192.168.17.128", 6379);

        jedis.auth("1234");
        Set<String> keys = jedis.keys("*");
        keys.forEach(System.out::println);

        jedis.close();
    }


    @Autowired
    RedisTemplate<Object,Object> redis;
    @Test
    void testSpringDataRedis() {
        redis.opsForValue().set("s12", "v1");
        Object o = redis.opsForValue().get("s12");
        System.out.println(o);

        redis.opsForHash().put("h1", "k2", "v11");
        Map<Object, Object> entries = redis.opsForHash().entries("h1");
        entries.entrySet().forEach(System.out::println);
    }
}
